<footer class="pt-3 pb-3">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 text-center">
                <p class="mb-0">Freelance Web Developer Nick Marsh © <?php echo date( 'Y' ); ?></p>
            </div>
        </div>
    </div>
</footer>

    </div> <!--end content-->
</div> <!--end wrapper-->