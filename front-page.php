<?php get_header(); ?>
<div class="inner">
    <div class="top-background back-dark d-flex justify-content-center flex-column">
        <div class="container">
            <div class="row flex-column">
                <div class="col-12">
                    <h1 class="text-uppercase headline-top">Nick <br>Marsh</h1>
                    <h2 class="headline-lower text-uppercase">Freelance Web Developer</h2>
                    <p>San Diego based web developer specializing in building and growing your brand.</p>
                    <a class="main-btn" href="#scroll-portfolio">Recent Projects</a>
                </div>
            </div>
        </div>
    </div>

    <section id="scroll-portfolio"> <!--begin portfolio-->
        <div class="container">
            <div class="row">
                <div class="col">
                    <h2 class="md-h2">PROJECTS</h2>
                </div>
            </div>
            <div class="row mt-3">
                <div class="project-item col-12 col-md-6 col-lg-4 mb-3">
                    <a target="_blank" href="https://projectbarandgrill.com/">
                        <div class="project-item-inner">
                            <div class="project-image">
                                <img class="img-fluid" alt="Project Bar & Grill"
                                     src="/wp-content/themes/base/assets/images/project.jpg">
                                <p class="project-name pt-3 pb-3 pl-3">Project Bar & Grill</p>
                            </div>
                            <div class="overlay d-flex flex-column justify-content-center pl-2">
                                <p class="project-name-overlay">Project Bar & Grill</p>
                                <p class="project-dev"><span class="font-weight-bold">Development:</span> PHP,
                                    JavaScript,
                                    HTML, CSS</p>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="project-item col-12 col-md-6 col-lg-4 mb-3">
                    <a target="_blank" href="https://acenta.com/">
                        <div class="project-item-inner">
                            <div class="project-image">
                                <img class="img-fluid" alt="ACENTA"
                                     src="/wp-content/themes/base/assets/images/acenta.jpg">
                                <p class="project-name pt-3 pb-3 pl-3">ACENTA</p>
                            </div>
                            <div class="overlay d-flex flex-column justify-content-center pl-2">
                                <p class="project-name-overlay">Arkansas Center for Ear, Nose, & Throat</p>
                                <p class="project-dev"><span class="font-weight-bold">Development:</span> PHP,
                                    JavaScript,
                                    HTML, Sass</p>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="project-item col-12 col-md-6 col-lg-4 mb-3">
                    <a target="_blank" href="http://gdtlab.com/">
                        <div class="project-item-inner">
                            <div class="project-image">
                                <img class="img-fluid" alt="Global Dental Technologies"
                                     src="/wp-content/themes/base/assets/images/gdtlab.jpg">
                                <p class="project-name pt-3 pb-3 pl-3">Global Dental Technologies</p>
                            </div>
                            <div class="overlay d-flex flex-column justify-content-center pl-2">
                                <p class="project-name-overlay">Global Dental Technologies</p>
                                <p class="project-dev"><span class="font-weight-bold">Development:</span> PHP,
                                    JavaScript,
                                    HTML, CSS</p>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="project-item col-12 col-md-6 col-lg-4 mb-3">
                    <a target="_blank" href="https://www.wandaspianoarts.com/">
                        <div class="project-item-inner">
                            <div class="project-image">
                                <img class="img-fluid" alt="Wanda's Piano Arts"
                                     src="/wp-content/themes/base/assets/images/wandaspiano.jpg">
                                <p class="project-name pt-3 pb-3 pl-3">Wanda's Piano Arts</p>
                            </div>
                            <div class="overlay d-flex flex-column justify-content-center pl-2">
                                <p class="project-name-overlay">Wanda's Piano Arts</p>
                                <p class="project-dev"><span class="font-weight-bold">Development:</span> PHP,
                                    JavaScript,
                                    HTML, Sass</p>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="project-item col-12 col-md-6 col-lg-4 mb-3">
                    <a target="_blank" href="http://bjdentallab.com/">
                        <div class="project-item-inner">
                            <div class="project-image">
                                <img class="img-fluid" alt="Beijing Dental Laboratory"
                                     src="/wp-content/themes/base/assets/images/bjdental.jpg">
                                <p class="project-name pt-3 pb-3 pl-3">Beijing Dental Laboratory</p>
                            </div>
                            <div class="overlay d-flex flex-column justify-content-center pl-2">
                                <p class="project-name-overlay">Beijing Dental Laboratory</p>
                                <p class="project-dev"><span class="font-weight-bold">Development:</span> PHP,
                                    JavaScript,
                                    HTML, CSS</p>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </section> <!--end portfolio-->

    <section id="scroll-services"> <!--begin services-->
        <div class="container">
            <div class="row">
                <div class="col-12 pb-4">
                    <h2 class="md-h2">SERVICES</h2>
                    <h3 class="intro-hd mb-4">START GROWING YOUR ONLINE WEB PRESENCE TODAY</h3>
                    <p>We'll put together a plan for design, development, launch, and post launch to help grow your
                        business. I'm
                        constantly devouring information on the lastest website design and development trends. Over the
                        years, the
                        rise
                        of smart phones and tablets have created a fundamental shift in website design. I'll ensure that
                        your new
                        website is built responsively so the user's experience on desktop, tablet, and mobile stays
                        great.
                    </p>
                </div>

                <div class="service-item col-12 col-md-6 pr-6">
                    <h4>DESIGN & DEVELOPMENT</h4>
                    <i class="fas fa-pen-fancy"></i>
                    <p>The most important step before we start design and development is the discussion about your
                        vision
                        and
                        goals for your new website. By doing this, I will be able to create a design that captures the
                        look,
                        feel, and ambitions of your site. Once you are happy with the design and it's approved, we move
                        into
                        the
                        development phase. During this phase I will bring the design to life. Everything I code will be
                        extensively tested in all browsers and on all devices.
                    </p>
                </div>
                <div class="service-item col-12 col-md-6">
                    <h4>SECURITY & MAINTENANCE</h4>
                    <i class="fas fa-lock"></i>
                    <p>I'm available to implement specific security measures to keep your site secure and to minimize
                        the
                        risk of your website getting hacked. Along with this, I'll be sure to keep everything on your
                        website up to date to ensure everything continues running smoothly.
                    </p>
                </div>
                <div class="service-item col-12 col-md-6 pr-6">
                    <h4>ECOMMERCE</h4>
                    <i class="fas fa-shopping-cart"></i>
                    <p>I have many years of experience building and managing eCommerce websites. I understand the
                        importance
                        of the user being able to find the products they are looking for as easily as possible.
                    </p>
                </div>

                <div class="service-item col-12 col-md-6">
                    <h4>CUSTOMIZATION</h4>
                    <i class="fas fa-laptop-code"></i>
                    <p>Already have a site? I have over 4 years of experience building and maintaining websites. If
                        you're
                        looking for some aesthetic touchups, bug fixes, or adding a robust feature, I'm your guy.
                    </p>
                </div>
                <div class="service-item col-12 col-md-6 pr-6">
                    <h4>HOSTING</h4>
                    <i class="fas fa-server"></i>
                    <p>Need hosting? I can provide a secure, fast, and reliable place to house your new website.

                    </p>
                </div>
                <div class="service-item col-12 col-md-6">
                    <h4>TRAINING</h4>
                    <i class="fas fa-users"></i>
                    <p>Eventually, you will want to edit, add, or remove content on your new website. I will make sure
                        to
                        create a highly customizable site and provide guidance so you can make edits to what you want,
                        when
                        you want.
                    </p>
                </div>
            </div>
        </div>
    </section> <!--end services-->

    <section id="scroll-testimonials"><!--begin testimonials-->
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h2 class="md-h2">TESTIMONIALS</h2>
                    <h3 class="intro-hd mb-4">WHAT MY CLIENTS ARE SAYING</h3>
                    <div id="testimonials-carousel" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <li data-target="#testimonials-carousel" data-slide-to="0" class="active"></li>
                            <li data-target="#testimonials-carousel" data-slide-to="1"></li>
                            <li data-target="#testimonials-carousel" data-slide-to="2"></li>
                        </ol>
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <p>"We (Sunday Scaries) began working with Nick in February 2018. Nick was a tremendous
                                    asset to our
                                    growing company. He was responsible for website optimization, website design and
                                    coordinating needs
                                    with 3rd party companies. Not only did he communicate in a very timely manner, but
                                    he
                                    also made
                                    proactive suggestions to the problems and challenges our startup encountered. For
                                    example, we hired
                                    Disruptive Advertising to split test different landing pages on our site. Based on
                                    monthly results,
                                    we needed coding and design work done to help optimize their findings to improve
                                    conversions. Nick
                                    was integral in that process of implementation, which drastically helped improve our
                                    ROI. We would
                                    highly recommend him for any prospective clients considering adding him to their
                                    teams."
                                </p>
                                <h4>Beau Schmitt
                                </h4>
                                <p>Co-Founder, SundayScaries.com, Projectbarandgrill.com (TQ Supplements LLC)</p>
                            </div>
                            <div class="carousel-item">
                                <p>"Nick was great to work with. He was able to understand
                                    exactly what we wanted and he also added some great feedback on how to improve
                                    things.
                                    Not only was
                                    he able to built us an amazing website, he was also able to provide us with SEO and
                                    affordable
                                    hosting.
                                    I highly
                                    recommend Nick for anything web related. I wouldn't hesitate to call him again if
                                    the
                                    need arises.
                                    Thanks Nick for all of your hard work!"</p>
                                <h4>Nick Martin</h4>
                                <p>CEO, Global Dental Technologies</p>
                            </div>
                            <div class="carousel-item">
                                <p>Nick Marsh recently created a website for my business. He responded to all of my
                                    questions and
                                    offered excellent suggestions every step of the way. I am very pleased with the
                                    results!
                                </p>
                                <h4>Wanda Kuntz</h4>
                                <p>Wanda's Piano Arts</p>
                            </div>
                        </div>
                        <a class="carousel-control-prev" href="#testimonials-carousel" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"> <i class="fas fa-chevron-left"></i>
                        </span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#testimonials-carousel" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"> <i
                                    class="fas fa-chevron-right"></i>
                        </span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section> <!--end testimonials-->

    <section id="scroll-contact">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <h2 class="md-h2">CONTACT</h2>
                </div>
                <div class="col-12 col-md-12 text-center">
                    <h4 class="pb-3 pt-3">CONNECT WITH ME</h4>
                    <a target="_blank" href="https://www.linkedin.com/in/nick-marsh-20161a89/"><i
                                class="fab fa-linkedin"></i></a>
                    <a target="_blank" href="https://bitbucket.org/nmarsh33/"><i class="fab fa-bitbucket"></i></a>

                </div>
                <div class="col-12">
					<?php gravity_form( 1, false, false, false, '', false ); ?>
                </div>
            </div>
        </div>
    </section> <!--end contact-->
</div>
<?php get_footer(); ?>

