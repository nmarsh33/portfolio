<?php
/**
 * Portfolio functions and definitions
 *
 *
 */

/**
 * Theme setup
 *
 */
require get_template_directory() . '/inc/setup.php';

/**
 * Load functions to secure your WP install.
 *
 */
require get_template_directory() . '/inc/security.php';

/**
 * Enqueue styles and scripts
 *
 */
require get_template_directory() . '/inc/enqueue.php';


/**
 * Register widget area.
 *
 */
require get_template_directory() . '/inc/widgets.php';

/**
 * Custom Post Types
 *
 */
require get_template_directory() . '/inc/custom-posts.php';

/**
 * Load custom WordPress nav walker.
 *
 */
require get_template_directory() . '/inc/bootstrap-wp-navwalker.php';

/**
 * Theme Settings
 *
 */
require get_template_directory() . '/inc/theme-options.php';


/**
 * Hero Manage
 *
 */
require get_template_directory() . '/inc/hero.php';










