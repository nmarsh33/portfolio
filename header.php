<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-title"
          content="<?php bloginfo( 'name' ); ?> - <?php bloginfo( 'description' ); ?>">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css">

	<?php wp_head(); ?>

</head>

<header class="d-none d-sm-block"> <!--begin header-->
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
				<?php
				wp_nav_menu( array(
					'theme_location' => 'primary',
					'container'      => false,
					'menu_class'     => 'main-nav d-flex justify-content-around'
				) );
				?>
            </div>
        </div>
    </div>
</header> <!--end header-->
<div class="wrapper"> <!--begin wrapper-->
    <div class='sidebar'> <!--mobile sidebar-->
		<?php
		wp_nav_menu( array(
			'theme_location' => 'primary',
			'container'      => false,
			'menu_class'     => 'side-nav'
		) );
		?>
    </div> <!--end mobile sidebar-->
    <div class="content"> <!--begin content-->
        <!-- Mobile nav button-->
        <a class='mobile-button nav-but d-block d-sm-none'>
            <i class="fas fa-bars"></i>
        </a> <!--mobile button-->
        <body <?php body_class(); ?>>

