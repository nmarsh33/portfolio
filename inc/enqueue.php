<?php
/**
 *
 * Styles: Frontend with no conditions, Add Custom styles to wp_head
 *
 * @since  1.0
 *
 */
add_action( 'wp_enqueue_scripts', 'port_styles' ); // Add Theme Stylesheet
function port_styles() {

	/**
	 *
	 * Minified and Concatenated styles
	 *
	 */
	wp_register_style( 'port_style', get_template_directory_uri() . '/style.min.css', array(), '1.0', 'all' );
	wp_enqueue_style( 'port_style' ); // Enqueue it!

	/*Font Awesome*/
	wp_enqueue_style( 'font-awesome', 'https://use.fontawesome.com/releases/v5.2.0/css/all.css' );
	/*Google Font*/
	wp_enqueue_style( 'wpb-google-fonts', 'https://fonts.googleapis.com/css?family=Montserrat:200,400,600,800', false );

}

/**
 *
 * Scripts: Frontend with no conditions, Add Custom Scripts to wp_head
 *
 * @since  1.0.0
 *
 */
add_action( 'wp_enqueue_scripts', 'medi_scripts' );
function medi_scripts() {
	if ( $GLOBALS['pagenow'] != 'wp-login.php' && ! is_admin() ) {

		wp_enqueue_script( 'jquery' ); // Enqueue it!

		/**
		 *
		 * Minified and concatenated scripts
		 *
		 * @vendors     plugins.min,js
		 * @custom      scripts.min.js
		 *
		 *     Order is important
		 *
		 */
		wp_register_script( 'port_bootstrapJS', 'https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js' );
		wp_enqueue_script( 'port_bootstrapJS' ); // Enqueue it!
		wp_register_script( 'port_customJS', get_template_directory_uri() . '/assets/js/custom.min.js#asyncload' ); // Custom scripts
		wp_enqueue_script( 'port_customJS' ); // Enqueue it!
	}

}